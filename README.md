# Superheroes
Web page... Superheroes... We should agree on a name for this app and write a nice description here.

## Getting started
A demo version, when available, will be hosted on a VPS whose address will be here when we set one up.
For a local build, refer to the Building section.

## Repository structure
Source files are found in the src directory, binaries in bin and documentation in doc.
The misc folder is supposed to contain miscellaneous files that need to be shared with the project team.

## Contribution
The [*WORKFLOW.md*](WORKFLOW.md) file contains a short set of instructions on how to contribute to this repository.

## Build requirements
Will be listed here when we agree on what they are.

## Building
Will be described here when we agree on how it should be done.

## Credits
We are all very grateful to our magnificent team leader who gives us points.
Other than that, no credits so far.

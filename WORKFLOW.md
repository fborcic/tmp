# Workflow
The purpose of this file is to give an overview on how to contribute to this repository
and how to use the Git versioning system in doing so.

**Please note:** there are many different approaches to using Git and GitLab. At the time
of writing, I haven't attended the lectures so far and have no idea if the method I describe
here is appropriate for our project. We will discuss this with our mentor and, should
any of this deviate from what is required, modify this file accordingly.

**Also note:** Here I suppose you are all using Windows and git from the git bash shell.
It is possible to use git from the native Windows shell, third-party GUI tools and
all popular IDEs, but I cannot possibly cover all that.

## First steps <a name="start"></a>
Download Git software from <http://git-scm.com> and run the installer.

When presented with this screen, be sure to check Git LFS. We might use that.

![](misc/guide_images/lfs.png)

Continue with the default options until the installer asks you for your default editor.
Then, if not familiar with Vim, choose something else. Notepad++ (as shown below) is
a good choice.

![](misc/guide_images/notepad.png)

Leave everything else as it is until the final screen. Then check "Launch git bash":

![](misc/guide_images/git_bash.png)

After finishing the instalation, a bash shell will pop up. Now is a good time to
set up your SSH keys for GitLab, to save you the hassle of entering your password
every time you push something up there.

To do so, enter `ssh-keygen` and press enter. You will be prompted for a storage location
and a passphrase (twice). Just leave all of those empty and press enter every time.
Your screen should look like this:

![](misc/guide_images/ssh_keygen.png)

Now type `cat .ssh/id_rsa.pub` and press enter. Your public SSH key should be printed.
Copy all this by selecting copy from the right click menu (Ctrl+C won't work).

![](misc/guide_images/copy_key.png)

After copying the key, go to <https://gitlab.com/profile/keys> and paste your key in the
appropriate field. Name it with a title and click on "Add key".

![](misc/guide_images/paste_key.png)

Now navigate to wherever you want your local copy of the repository to reside
using the `cd` command, but beware that the path specs in git bash are a bit weird. 
For example, if you wanted to navigate to `C:\Users\Mirko\Documents`,
you would type `cd /c/Users/Mirko/Documents`. You can see your current location
in the window title bar.

When in the appropriate place, type `git clone git@gitlab.com:fborcic/ferovci.git`.
Type `yes` when warned about adding a new host and press enter.
 
That's it, you should be all set now.

## Basic workflow

To illustrate the workflow, let's add a dummy "feature" to our repository;
in this example, we'll make an interface that allows the user to select a
gender for their superhero.

To do so, we first need to create an issue.

### Creating an issue

GitLab has an integrated issue tracking system. An issue is a description
of a task that needs to be done, along with some additional data about its
complexity, importance and status.

To open the issue tracker, click on the "Issues" item in the GitLab sidebar.
A list of issues should open up. Above it, there is a "New issue" button.
Press it to create a new issue.

![](misc/guide_images/issues.png)

Let's file an issue for our gender picker:

![](misc/guide_images/new_issue.png)

Every issue should have a title and a detailed description of what needs to be done.
Besides that, when filing a new issue, you should select a due date, the appropriate milestone
(in our case the week when it should be done) and some labels.

There are two default labels (*To do* and *Doing*) which correspond to the issue status. If we agree
an issue should be resolved, it should get a todo label. Here I selected it already,
but it is possible to assign labels to an issue afterwards.

Apart from these two labels, I've created four more to categorize the issues.
Those are *Doc issue*, *Enhancement*, *Defect* and *Misc*, which are hopefully
self-explanatory. When opening issues, please label them with one of those.
This issue will introduce a new "feature", so I labeled it *Enhancement*.

The issue can be assigned to somebody at the time of creation or later.

After filling out this form and clicking on submit, a new issue will be created.

### Starting work

To start work on an issue that has a *To do* label, go to **Issues > Board** in the sidebar.
There you'll see all our issues organized into four columns (Open, To Do, Doing and Closed).

Our issue is now in the second column, so we drag it to the third one to change its status 
to *Doing*. That will indicate someone is working on it.

![](misc/guide_images/board.png)

After that, we can start working on the issue. When starting work on a feature, it is customary
to create a separate branch for the development of that particular feature. GitLab can automatically
do that for us, so let's use that.

To open a feature branch (and the corresponding MR), click on the issue title on the board to open it.
Then, click on the arrow next to **Create merge request** and
replace the default name of the branch with something shorter,
but leave the number prefix so we know which branch corresponds to which issue.
Leave everything else as it is and click **Create merge request**.

![](misc/guide_images/create_mr.png)

We have now created a merge request and a branch for our feature. A merge request is a proposal
to merge a feature branch into one of the main branches. We will merge our feature branches
into our development branch, and then merge the development branch into master every now and then
when we want to "release" our changes.

I will talk about MRs in more detail later. For now, let's just "checkout" our brand new branch
(which is, for now, identical to the development branch), and start writing code.

**Note:** From now on, we will be using git. Git commands will only work from inside the repository,
so if you followed the instructions from [First steps](#start), you would open git bash
and `cd` to somewhere inside the repository to work with git.

Our newly created branch (named 3-genderpicker) currently only exists on the GitLab server and not in our local
repository. We need to explicitly make it available locally. To do so, we run the following two commands:
```
git fetch
git checkout -b 3-genderpicker origin/3-genderpicker
```

The first command tells our local git to get info about what has changed on the remote (GitLab) server,
while the second one creates a branch named 3-genderpicker on our computer, links it with the remote
branch of the same name and switches to it.

Now we can start writing code.

### Adding a file

Let's start by adding a basic, first version of our gender picker.

We'll open a text editor and write some code, then save it in the repository
in the appropriate location. Since this is a demo unrelated to our project, we'll
save it under `ferovci/misc/genderpicker`:

![](misc/guide_images/html1.png)

Opening that in a browser to see the results:

![](misc/guide_images/html1r.png)

Now our code is saved, but git will ignore the file unless we tell it not to.
If we run `git status`, it will warn us about that, saying that there are untracked
files present.

```
Frans-MacBook-Pro:ferovci fran$ git status
On branch 3-genderpicker
Your branch is up to date with 'origin/3-genderpicker'.

Untracked files:
  (use "git add <file>..." to include in what will be committed)

        misc/genderpicker/

nothing added to commit but untracked files present (use "git add" to track)
```

We'll tell git to track the file by running `git add misc/genderpicker/slider.html`
from the root of our repository. If we were positioned inside the *genderpicker* directory,
we would run `git add slider.html`, or `git add *` to add everything inside that directory.

Let's run `git status` again:
```
Frans-MacBook-Pro:ferovci fran$ git status
On branch 3-genderpicker
Your branch is up to date with 'origin/3-genderpicker'.

Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

        new file:   misc/genderpicker/slider.html
```

Git now shows that the file is staged and that it will be included in our next commit. A commit
is a change in code of any kind, that has a short description, timestamp, id, author and so on.

This is a good time to commit our changes. To commit, run `git commit`.
This will open the editor you set as default when installing git to allow you to write a commit
message. A commit message is a *short* description of what your change introduces to the code.
If you feel that it needs to be more descriptive, leave a blank line after your short commit message
and then write a more descriptive one underneath. For example:

![](misc/guide_images/commit_msg.png)

We save that file and close the editor. Now our changes are committed in your local repository, 
but don't exist on the server yet. To publish them, we have to run `git push`.

After that, our changes are safely stored on the server. We can call it a day now and go
get a beer.

### Developing further

After sobering up, we want to improve our new feature. Let's add some styling so it's not that ugly.
Keep in mind I'm terrible at this so it might become uglier yet.

Anyways, let's write a css stylesheet and save it under `ferovci/misc/genderpicker/slider.css`:

![](misc/guide_images/css.png)

We need to link the stylesheet with our html file, so we'll also edit slider.html:

![](misc/guide_images/html2.png)

Now it looks ~~a bit better~~ different:

![](misc/guide_images/html2r.png)

Let's run `git status` now:

```
Frans-MacBook-Pro:genderpicker fran$ git status
On branch 3-genderpicker
Your branch is up to date with 'origin/3-genderpicker'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        modified:   slider.html

Untracked files:
  (use "git add <file>..." to include in what will be committed)

        slider.css

no changes added to commit (use "git add" and/or "git commit -a")
```

Now, since we already told git to track our slider.html file, it recognizes a change
and warns us that it's been modified, but that it isn't staged (won't be added in the next commit).
It also warns us that there is a file it doesn't care about, named slider.css.

To tell it to care, as before, we have to run `git add slider.css`.
Now slider.css has been staged for the next commit, but slider.html still hasn't.
We could run `git add slider.html` to stage it, but since it is already tracked, we can use
a shortcut. We'll also use a shortcut for the commit message, so we don't have to edit
it in an external editor. So, here's the command:

```
git commit -am "Added styling to gender picker"
```

The `-a` flag tells git to stage all modified tracked files before committing,
the `-m` flag followed by a message specifies the commit message. `-am` is just a
shorthand way to write `-a -m`.

We follow that by a `git push`, and our new code is once again uploaded.

### Merge request

We are quite satisfied with our feature now and want to integrate it into the development branch.
As you recall, we already have a merge request proposing to do so. You'll notice that the title of
that merge request starts with "WIP:". That stands for "Work In Progress" and prevents merging of the
branch until the feature is complete. We think it should be complete now, so we "Resolve WIP status".

![](misc/guide_images/resolve_wip.png)

Next, we assign somebody to do the review and eventually merge the branch. We do that by editing
the "Assignee" field in the upper right corner.

I'll be the reviewer for the purposes of this demo. If I weren't the one writing the code,
I'd checkout the branch using `git fetch`, followed by `git checkout -b 3-genderpicker origin/genderpicker`
to have the code available on my own machine. Since I am the one who has written it, I already have the
branch locally. I will open the *slider.html* file and test the functionality, concluding
that the feature is too politically correct. Then, I'll start a discussion on the merge request concerning
that. To do so, I'll click on "Changes", find the corresponding lines in the code, and click
on the line number to leave a comment:

![](misc/guide_images/review.png)

The developer (who is unfortunately also me in this case) will now see that I've started a discussion.
The merge request cannot be merged until all discussions have been resolved.

So, to address the complaint, we will now add some code in our *slider.html* file to
make it more politically incorrect:

![](misc/guide_images/html3.png)

Now, we commit it again using `git commit -am "Made the gender picker normal"`, and follow that
by `git push` to upload. We click on resolve discussion on the merge request and wait for the reviewer to
comment further or merge the branch.

Back in the shoes of a reviewer, I see no further problems with the code and click on merge.

You will likely encounter some problems with those merges because of so called merge conflicts
and because there are some different merging strategies, but we'll address them when they appear.
In this case, the merge is straightforward. After clicking on merge, our feature becomes available
on the development branch. Also, GitLab automatically closes the resolved issue.

To see that it has been incorporated, we switch to the development branch by running
`git checkout development` and then running `git pull`. The pull command is, sensibly,
the oposite of the push command. It downloads the changes from a server and merges them
into your local, currently active branch.

You'll find yourself often running these two commands, since by doing this you
effectively download the last development version of our project.

If you run `git log` now, you will see our commits on the development branch.

## Git
We have now implemented our first feature, and I hopefully illustrated some of the concepts
in doing so. This doesn't begin to cover git's features, and I suggest you look
for some tutorial on git to learn the basics. It gets very intuitive once you get ahold
of the basic concepts.

Here are some (unmentioned) commands you'll probably be using at some point:
* `git reset`
* `git rebase -i` **(important)**
* `git commit --amend`
* `git push -f`/`git push --force-with-lease`
* `git diff`
* `git log`
* `git reflog` **(useful for undoing almost everything)**
* `git stash`
* `git cherry-pick`
* `git blame`

`git rebase -i` is used to rewrite history. It is particularly useful when
responding to a code review comment on a merge request. If neccessary, I will explain
how to use it when we start writing actual code.

## That's about it
I'm terribly tired, so I apologize if this isn't too clear. If you're having trouble understanding
something, message me and we'll clear it up.

You can actually find the "Gender picker" in `misc/genderpicker` on the development branch, if
you want to see it in action.
